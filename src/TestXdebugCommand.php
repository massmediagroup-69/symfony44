<?php


namespace App;

use App\Message\MyMessage;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class TestXdebugCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;
    /**
     * @var MessageBusInterface
     */
    private MessageBusInterface $messageBus;

    public function __construct(EntityManagerInterface $entityManager, MessageBusInterface $messageBus, string $name = null)
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
        $this->messageBus = $messageBus;
    }

    public function configure()
    {
        $this->setName('bla:bla');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->messageBus->dispatch(new MyMessage('ogogogo'));
    }
}
