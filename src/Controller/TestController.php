<?php


namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class TestController extends AbstractController
{
    private $a = 4;
    /**
     * @Route("/api/public/test")
     * @return string[]
     */
    public function test(
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $passwordEncoder
    ) {
//        dump(1);

        $user = new User();
        $user->setEmail('olegnovatskiy3@gmail.com');
        $user->setName('olegnovatskiy');
        $user->setPassword($passwordEncoder->encodePassword($user, 'qweqwe'));
        $entityManager->persist($user);
        $entityManager->flush();
//
//        /** @var EntityRepository $userRepository */
        $userRepository = $entityManager->getRepository(User::class);

        dump(  $userRepository->findAll());

        return ['user' =>  $userRepository->findAll()];
    }

    /**
     * @Route("/api/test")
     * @return string[]
     */
    public function secureTest()
    {

        //$this->a = 10;
        $a =1;


        $b = 2;

        return ['secure routew' =>  $a + $b];
    }
}
