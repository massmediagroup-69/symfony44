<?php


namespace App\Listener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ViewEvent;

class ApiControllerJsonListener
{
    public function onKernelView(ViewEvent $event)
    {
        $event->setResponse(new JsonResponse($event->getControllerResult()));
    }
}
